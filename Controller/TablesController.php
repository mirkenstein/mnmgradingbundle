<?php

namespace Mnm\MnmGradingBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Mnm\MnmBluestemBundle\Entity\AuthorizedUsers;
use Symfony\Component\HttpFoundation\JsonResponse;

class TablesController extends Controller {

    public function indexAction() {
        return array();
    }

    /**
     * @Route("/roster", name="mnm_grading_table")
     * @Template()
     */
    public function tableAction() {

        $query = array(
            array('$match' => array('Exams.CRN' => array('$ne' => null, '$ne' => ''))),
            array('$group' => array('_id' => '$Exams.CRN'))
        );

        $crns = $this->aggregateMongoData($query);

        return array("crns" => $crns);
    }

    /**
     * @Route("/grading", name="mnm_grading_grading")
     * @Template()
     */
    public function gradingAction() {
        $username = $this->getUser()->getUsername();

        $crns = $this->getUserCrns($username);
        $type = $this->getClassType($username);
        $collection = $this->getMongoCollection('classData');
        $query = array($type => 1, 'user' => 1, '_id' => 0);
        $sort = array("user.CRN" => 1, "user.last_name" => 1);

//       print_r($crns);        
        $criteria = array(
            "$type.CRN" => array('$in' => $crns)
        );

        $roster = $this->getMongoData($criteria, $query, $collection, $sort);

        return array('roster' => $roster);
    }

    /**
     * @Route("/scores", name="mnm_grading_scores")
     * @Template()
     */
    public function scoresAction() {
        $username = $this->getUser()->getUsername();
        $em = $this->getDoctrine()->getManager();

        //Mongodb
        $collection = $this->getMongoCollection('classData');

        $query = array('extra' => 0, '_id' => 0, 'username' => 0);
        $criteria = array(
            "username" => $username
//            "table" => array('$ne' => "Roster")
        );
        $sort = array();
        $scores = $this->getMongoData($criteria, $query, $collection, $sort);
//                print_r($scores);die;
//        $queryExam1 = array(
//            array('$project' => array('_id' => '$Exams.scores.exam_1')),
//            array('$match' => array('_id' => array('$gt' => 0))),
//            array('$sort' => array('_id' => 1))
//        );
//        $queryExam2 = array(
//            array('$project' => array('_id' => '$Exams.scores.exam_2')),
//            array('$match' => array('_id' => array('$gt' => 0))),
//            array('$sort' => array('_id' => 1))
//        );
//
//        $queryExam3 = array(
//            array('$project' => array('_id' => '$Exams.scores.exam_3')),
//            array('$match' => array('_id' => array('$gt' => 0))),
//            array('$sort' => array('_id' => 1))
//        );
//
//        $histVals1 = $this->aggregateMongoData($queryExam1);
//        $histVals2 = $this->aggregateMongoData($queryExam2);
//        $histVals3 = $this->aggregateMongoData($queryExam3);
//        $histVals = array($histVals1, $histVals2, $histVals3);
//        $histogram = $this->getMongoHistogram();
//        return array('scores' => $scores, 'histVals' => $histVals);
//        
//        
        //Get the Lab CRN nr and check the week day and time
        /*
          $crn = $scores[0]["Labs"]["CRN"];
          $em = $this->getDoctrine()->getManager();
          $crnDetails = $em->getRepository('HomeBundle:CrnList')->findOneByCrn($crn);
          print_r($crnDetails->getWeekday() . $crnDetails->getStartTime());

          //sanie query as in the Quiz controller getUserHwDue
          $query = $em->createQuery(
          'SELECT p.endDate, p.hwDue
          FROM HomeBundle:Syllabus p
          WHERE p.endDate > :date
          AND  p.hwDue IS NOT NULL
          ORDER BY p.endDate ASC
          '
          );
          $query->setParameter('date', new \DateTime("now"));
          $query->setMaxResults(1);
          $result = $query->getSingleResult();
          print_r($result);
          unset($scores[0]["Homework"]["scores"]["hw_2"]);

         */
        return array('scores' => $scores);
    }

    /**
     * @Route("/mongo", name="mnm_grading_mongo")
     * @Template()
     */
    public function mongoAction(Request $request) {
        //get the appropriate table
        $table = $request->get('table');
        $crns = $request->get('crns');

        if (is_array($crns)) {
            $crns = array_map('intval', $crns);
            //Bad design. Need to put the CRN in the Roster Table
            $criteria = array(
                ($table == "Roster" ? "user" : "$table") . ".CRN" => array('$in' => $crns)
            );

            $sort = array(($table == "Roster" ? "user" : "$table") . ".CRN" => 1, "user.last_name" => 1);
        } else {
            $criteria = array();
            $sort = array("user.CRN" => 1, "user.last_name" => 1);
        }

        $collection = $this->getMongoCollection('classData');
        $query = array($table => 1, 'user' => 1, '_id' => 0);


        return $this->getJsonMongoData($criteria, $query, $collection, $sort);
    }

    /**
     * @Route("/mongoremove", name="mnm_grading_remove")
     */
    public function removeAction(Request $request) {
        $userlist = $request->get('userlist');
        $criteria = array();
        $collection = $this->getMongoCollection('classData');
        $query = array('username' => array('$in' => $userlist));
        return $this->removeMongoData($criteria, $query, $collection);
    }

    /**
     * @Route("/mongosave", name="mnm_grading_mongosave")
     */
    public function mongosaveAction(Request $request) {
        //mongodb init
        $collection = $this->getMongoCollection('classData');

//we are sendig JSON string 
        $phpData = $this->jsonDecodeRequest($request);
        $table = $phpData['table'];


        //use username (netid) field for identifying the  record.
        foreach ($phpData['val'] as $item) {

            $username = $item['user']['username'];
            $username = trim($username, chr(0xC2) . chr(0xA0));
            $username = trim($username);

            if (!empty($username)) {
                $item["user"]["username"] = $username; //tmp fix better work with the outer username 
                $collection->update(array("username" => $username), array('$set' => $item), array('upsert' => true));
            }
        }

        return new JsonResponse(array("result" => "ok"));
    }

    /**
     * @Route("/mongoauthorize", name="mnm_grading_mongoauthorize")
     */
    public function mongoauthorizeAction(Request $request) {

        //////////////////////
        //Set all the authorized flags to 0 in the AurorizedUsers Bluestem Table
        $em = $this->getDoctrine()->getManager();
        $conn = $this->get('database_connection');
        $statement = $conn->prepare('UPDATE authorized_users SET  is_authorized=0');
        $statement->execute();

        //MONGO 
        $collection = $this->getMongoCollection('classData');
        $criteria = array();
        $query = array('_id' => 0, 'user' => 1, 'username' => 1);
        $sort = array("username" => 1);
        $rows = $this->getMongoData($criteria, $query, $collection, $sort);

        foreach ($rows as $row) {
            $username = $row["username"];
            $crn = $row["user"]["CRN"];
            $dbUser = $em->getRepository("MnmBluestemBundle:AuthorizedUsers")->findOneByUsername($username);
            if (!$dbUser) {
                $dbUser = new AuthorizedUsers();
                $dbUser->setUsername($username);
                $dbUser->setIsAuthorized(1);
                $dbUser->setCrn($crn);
            } else {
                $dbUser->setIsAuthorized(1);
                $dbUser->setCrn($crn);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($dbUser);
            $em->flush();
//              unset($row[NULL]);
            ///Insert the row in MongoDb

            $row = array();
        }
        return new JsonResponse(array("result" => "ok"));
    }

    /**
     * @Route("/mongomerge", name="mnm_grading_merge")
     */
    public function mergeAction() {
        $collection = $this->getMongoCollection('quizAnswers');
        $query = array('_id' => 0, 'user' => 1);
        $cursor = $collection->find(array(), $query);

//        {$group:{_id:"$user",total:{$sum:"$isCorrect"}}}
        $ops = array(
            array(
                '$group' => array(
                    '_id' => array(
                        'user' => '$user',
                        'quiz' => '$Quiz'
                    ),
                    'total' => array('$sum' => '$isCorrect')
                )
            )
        );

        $answers = $collection->aggregate($ops);
//        print_r($answers);
//        $answers = iterator_to_array($cursor);
        $collection = $this->getMongoCollection('classData');
        $cursor = $collection->find(array(), $query);
        $roster = iterator_to_array($cursor);

        $merged = array();

        foreach ($roster as $person) {
//            print_r($person['user']);
            foreach ($answers['result'] as $ans) {

                if ($ans["_id"]['user'] === $person["user"]["username"]) {
                    $person = array_merge($person, array("Homework" =>
                        array(
                            "CRN" => $person["user"]["CRN"], "scores" => array("hw_" . $ans['_id']['quiz'] => $ans['total']))
                            )
                    );
                    $hwScore = array("Homework" => array("scores" => array("hw_" . $ans['_id']['quiz'] => $ans['total'])));


                    //Upsert that record to mongo db
                    //                    $collection->update(array("user" => $person["user"]), array('$set' => $person), array('upsert' => true));

                    $collection->update(array("user" => $person["user"]), array('$set' => array(
                            "Homework.scores.hw_" . $ans['_id']['quiz'] => $ans['total']
                            , "Homework.CRN" => $person['user']["CRN"]
                        )
                            )
                            , array('upsert' => true)
                    );
                }
            }
            // array_push($merged, $person);
        }

//        $res = array_merge($roster, $answers);
        $response = new JsonResponse();
        $response->setData(array(
            'data' => $merged
                )
        );
        return $response;
    }

    /**
     * @Route("/safetyquizlist", name="mnm_grading_safety_quiz_list")
     * @Template()
     */
    public function tableQuizSafetyAction() {
        $collection = $this->getMongoCollection('radSafetyData');
        $query = array('_id' => 0);
        $cursor = $collection->find(array(), $query);
        $list = iterator_to_array($cursor);
        return array('list' => $list);
    }

    /**
     * @Route("/getsafetyquizlist", name="mnm_grading_get_quiz_safety_list")
     */
    public function quizSafetyAction() {
        $collection = $this->getMongoCollection('radSafetyData');
        $query = array('_id' => 0);
        $cursor = $collection->find(array(), $query);
        $data = iterator_to_array($cursor);
        $response = new JsonResponse();
        $response->setData($data);
        return $response;
    }

}
