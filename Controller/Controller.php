<?php

namespace Mnm\MnmGradingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * db.classData.aggregate([{$group:{_id:null,total:{$sum:"$Exams.exam_1"}}}])

 */
class Controller extends BaseController {

    /**
     * @return \Symfony\Component\Security\Core\SecurityContext
     */
    protected function getSecurityContext() {
        return $this->container->get('security.context');
    }

    /**
     * @return \Mnm\MnmUserBundle\Entity\User
     */
    public function getUser() {
        return parent::getUser();
    }

    public function unsetArrayByKeys($inputArr, $keysArray) {
        $res = array();
        foreach ($inputArr as $item) {
            foreach ($keysArray as $key) {
                unset($item[$key]);
            }
            array_push($res, $item);
        }

        return $res;
    }

    public function getUserCrns($username) {
        $em = $this->getDoctrine()->getManager();
        $crns = $em->getRepository("HomeBundle:CrnList")->findAllCrnsByUsername($username);
        return $crns;
    }

    public function getClassType($username) {
        $em = $this->getDoctrine()->getManager();
        $person = $em->getRepository("HomeBundle:People")->findOneBy(array("username"=>$username));
        return $person->getGroupType()->getShortName();
    }

    /**
     * 
     * @param type $collection
     * @return type mongodb collection
     */
    public function getMongoCollection($collection) {
        $m = $this->container->get('doctrine_mongodb.odm.default_connection');
        $mongoDatabase = $this->container->getParameter('mongodb_database');
        $db = $m->selectDatabase($mongoDatabase);
        return $db->$collection;
    }

    /**
     * 
     * @param type $request
     * @return type json $phpData
     */
    public function jsonDecodeRequest($request) {
        $postedData = $request->getContent();
        return json_decode($postedData, true);
    }

    /**
     * 
     * @param type $criteria
     * @param type $query
     * @param type $collection
     * @return type array()
     */
    public function getMongoData($criteria, $query, $collection,$sort) {
        $cursor = $collection->find($criteria, $query)->sort($sort);
        return iterator_to_array($cursor);
    }

    public function getMongoHistogram() {
        $query = array(
            array('$project' => array('usr' => '$Exams.scores.exam_1')),
            array('$match' => array('usr' => array('$gt' => 0))),
            array('$project' => array('diff' => array('$subtract' => array('$usr', array('$mod' => array('$usr', 5)))))),
            array('$group' => array('_id' => '$diff', 'count' => array('$sum' => 1))),
            array('$sort' => array('_id' => 1))
        );
        $collection = $this->getMongoCollection('classData');
        $cursor = $collection->aggregate($query);

        $histCats = $this->flattenArray($cursor, "_id");
        $histVals = $this->flattenArray($cursor, "count");
        return new JsonResponse(array($histCats, $histVals));
//([
//{$project:{usr:"$Exams.scores.exam_1"}},
//{$match:{usr:{$gt:0}}},
//{$project:{diff:{$subtract:["$usr",{$mod:["$usr",5]}]}}},
//{$group:{_id:"$diff",count:{$sum:1}}}
//])
    }

    /**
     * 
     * @param type $criteria
     * @param type $query
     * @param type $collection
     * @return JsonResponse
     */
    public function getJsonMongoData($criteria, $query, $collection, $sort) {
        $cursor = $collection->find($criteria, $query)->sort($sort);
        $response = new JsonResponse();
        $response->setData(
                array(
                    'data' => iterator_to_array($cursor)
                )
        );
        return $response;
    }

    /**
     * 
     * @param type $query
     * @return type array()
     */
    public function aggregateMongoData($query) {
        $collection = $this->getMongoCollection('classData');
        $cursor = $collection->aggregate($query);
        return $this->flattenArray($cursor, "_id");
    }

    public function removeMongoData($criteria, $query, $collection) {
        $cursor = $collection->remove($query);
        return new JsonResponse($cursor);
    }

    /**
     * 
     * @param type $cursor description with result key from mongodb query
     * @return array
     */
    public function flattenArray($cursor, $key) {
        $array = array();
        foreach (array_values($cursor['result']) as $val) {
            array_push($array, $val[$key]);
        }
        return $array;
    }

}
