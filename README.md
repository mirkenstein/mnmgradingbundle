Mnm Grading Bundle
========================

This document contains information on how to setup this  bundle.


1) Install as git submodule in you project src dir
------------------------------------------------------
From the root of your project add the submodule.  

           git submodule add  git@bitbucket.org:mirkenstein/mnmgradingbundle.git ./src/Mnm/MnmGradingBundle
           git submodule add   https://mirkenstein@bitbucket.org/mirkenstein/mnmgradingbundle.git ./src/Mnm/MnmGradingBundle

2) Add in your project `composer.json` these two bundles
--------------------------------------------
    "require": {
            //...
        "doctrine/mongodb-odm": "1.0.*@dev",
        "doctrine/mongodb-odm-bundle": "3.0.*@dev"
          }

3) Register the bundles to your application kernel
--------------------------------------------
        // app/AppKernel.php
        public function registerBundles()
        {
            return array(
                // ...
                new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
                new Mnm\MnmGradingBundle\MnmGradingBundle(),
                // ...
            );
        }


3) Update your routing 
--------------------------------------------
      //app/config/routing.yml
        mnm_grading:
            resource: "@MnmGradingBundle/Controller"
            prefix:   /
            type: annotation  
     

3) Update parameters.yml and parameters.yml.dist
---------------------------------------------------------------
		parameters:
		      mongodb_host: 127.0.0.1
		      mongodb_port: 27017
		      mongodb_collection: myDBcollection

5) Add in your main config.yml the relevant settings for mongodb 
---------------------------------------------------------------
		doctrine_mongodb:
			connections:
				default:
					server: mongodb://%mongodb_host%:%mongodb_port%
					options: {}
			default_database: test_database
			document_managers:
				default:
					auto_mapping: true   

5) Override this bundle from one of your own bundles
--------------------------------------------
[See this cookbook entry ](http://symfony.com/doc/2.3/cookbook/bundles/inheritance.html)

	// src/Acme/MyownBundle/AcmeMyownrBundle.php
	namespace Acme\MyownBundle;

	use Symfony\Component\HttpKernel\Bundle\Bundle;

	class AcmeMyownrBundle extends Bundle
	{
	    public function getParent()
	    {
		return 'MnmGradingBundleBundle';
	    }
	}