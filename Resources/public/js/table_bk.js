var $container = $("#example1");
var $console = $("#example1console");
var $parent = $container.parent();
var autosaveNotification;
var cellProperties = {};
var dynamicColumns = [];

var defaultColumns = [{
        data: "last_name",
        title: "Last Name "

    }, {
        data: "first_name",
        title: "First Name"

    }, {
        data: "username",
        title: "netid"
    }, {
        data: "UIN",
        type: 'numeric',
        title: "UIN"
    }, {
        data: "CRN",
        type: 'numeric',
        title: "CRN"

    }];

dynamicColumns = defaultColumns;



$container.handsontable({
    startRows: 8,
    startCols: 6,
    rowHeaders: true,
//    colHeaders: ["Last Name ", "First Name ", "Username ", "UIN", "CRN"],
    minSpareRows: 1,
    columns: dynamicColumns,
//    columns: [{
//            data: "last_name"
//
//        }, {
//            data: "first_name"
//
//        }, {
//            data: "username"
//
//        }, {
//            data: "UIN",
//            type: 'numeric'
//        }, {
//            data: "CRN",
//            type: 'numeric'
//
//        }],
    contextMenu: true,
    columnSorting: {
        column: 4 //CRN column
    }
//    cells: cellProperties
    ,
    afterChange: function(change, source) {
        if (source === 'loadData') {
            return; //don't save this change
        }
        if ($parent.find('input[name=autosave]').is(':checked')) {
            clearTimeout(autosaveNotification);
            $.ajax({
                url: "mongosave",
                dataType: "json",
                type: "POST",
                data: JSON.stringify({
                    'saveall': false,
                    'val': handsontable.getDataAtRow(change[0][0])
                }),
                contentType: "application/json",
                //data: handsontable.getDataAtRow(change[0][0])

                complete: function(data) {
                    $console.text('Autosaved (' + change.length + ' ' +
                            'cell' + (change.length > 1 ? 's' : '') + ')');
                    autosaveNotification = setTimeout(function() {
                        $console.text('Changes will be autosaved');
                    }, 1000);
                }
            });
        }

    }
//    ,
//    beforeRemoveRow: function(change) {
//        console.log(handsontable);
//
//    }
});
var handsontable = $container.data('handsontable');

$parent.find('button[name=load]').click(function() {
    $('button[name=save]').show();
    if (dynamicColumns.length != 5) {
        dynamicColumns = dynamicColumns.slice(0, 5);
    }
    $container.handsontable("updateSettings", {columns: dynamicColumns, readOnly: false});
    handsontable.render();
    $.ajax({
        url: "mongo",
        dataType: 'json',
        type: 'GET',
        success: function(res) {
            $console.text('Data loaded');
            handsontable.loadData(res.data);
//            console.log(res);
        }
    });
});


$parent.find('button[name=loadhwscore]').click(function() {
    $('button[name=save]').hide();
    if (dynamicColumns.length == 5) {
        for (var i = 1; i <= 5; i++) {
            col = new Object();
            col.data = "total_quiz_" + i.toString();
            col.title = "HW" + i.toString();
            col.type = "numeric";
            col.readOnly = true;
            dynamicColumns.push(col);
        }
    }
    $container.handsontable("updateSettings", {columns: dynamicColumns, readOnly: true});
    handsontable.render();
    $.ajax({
        url: "mongomerge",
        dataType: 'json',
        type: 'GET',
        success: function(res) {
            $console.text('Data loaded');
            handsontable.loadData(res.data);
//            console.log(res);
        }
    });
});


$parent.find('button[name=save]').click(function() {
    $.ajax({
        url: "mongosave",
        data: JSON.stringify({
            'saveall': true,
            'val': handsontable.getData()
        }),
        // data: {"data": handsontable.getData()}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function(res) {

            if (res.result === 'ok') {
                $console.text('Data saved');
            }
            else {
                $console.text('Save error');
            }
        },
        error: function() {
            $console.text('Save error!!!');
        }
    });
});

$parent.find('input[name=autosave]').click(function() {
    if ($(this).is(':checked')) {
        $console.text('Changes will be autosaved');
    }
    else {
        $console.text('Changes will not be autosaved');
    }
});

