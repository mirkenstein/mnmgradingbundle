var $container = $("#example1");
var $console = $("#example1console");
var $parent = $container.parent();
var autosaveNotification;
var cellProperties = {};
var dynamicColumns = [];

activeTab = $(".nav-tabs li.active a").text();
var defaultColumns = [{
        data: "last_name",
        title: "Last Name "

    }, {
        data: "first_name",
        title: "First Name"

    }, {
        data: "username",
        title: "netid"
    },
    {
        data: "uin",
        type: 'numeric',
        title: "UIN"

    },
    {
        data: "class_number",
        type: 'numeric',
        title: "Class"

    },{
        data:"timestamp",
        title:"datetime"
        
        
    }];
var defaultSettings = {
    startRows: 8,
    startCols: 6,
    rowHeaders: true,
    minSpareRows: 1,
    columns: dynamicColumns,
    contextMenu: true,
    columnSorting: {
        column: 0,
        sortOrder: true
    },
};
dynamicColumns = defaultColumns;


$container.handsontable({
    startRows: 8,
    startCols: 6,
    rowHeaders: true,
    data: list,
    minSpareRows: 1,
    columns: dynamicColumns,
    contextMenu: true,
    columnSorting: {
        column: 4 //CRN column
    }


});
var handsontable = $container.data('handsontable');



