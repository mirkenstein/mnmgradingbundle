var $container = $("#example1");
var $console = $("#example1console");
var $parent = $container.parent();
var autosaveNotification;
var cellProperties = {};
var dynamicColumns = [];

activeTab = $(".nav-tabs li.active a").text();
var defaultColumns = [{
        data: "last_name",
        title: "Last Name "

    }, {
        data: "first_name",
        title: "First Name"

    }, {
        data: "username",
        title: "netid"
    }, {
        data: "UIN",
        type: 'numeric',
        title: "UIN"
    }, {
        data: "CRN",
        type: 'numeric',
        title: "CRN"

    }];
var defaultSettings = {
    startRows: 8,
    startCols: 6,
    rowHeaders: true,
    minSpareRows: 1,
    columns: dynamicColumns,
    contextMenu: true,
    columnSorting: {
        column: 0,
        sortOrder: true
    },
};
dynamicColumns = defaultColumns;


$container.handsontable({
    startRows: 8,
    startCols: 6,
    rowHeaders: true,
    minSpareRows: 1,
    columns: dynamicColumns,
    contextMenu: true,
    columnSorting: {
        column: 4 //CRN column
    }
    ,
    afterChange: function(change, source) {
        var table = $(".nav-tabs li.active a").text();
        if (source === 'loadData') {
            return; //don't save this change
        }
        var index = handsontable.sortIndex[change[0][0]][0];
        if ($parent.find('input[name=autosave]').is(':checked')) {
            clearTimeout(autosaveNotification);
            $.ajax({
                url: "mongosave",
                dataType: "json",
                type: "POST",
                data: JSON.stringify({
                    saveall: false,
                    table: table,
                    val: handsontable.getDataAtRow(index)
                }),
                contentType: "application/json",
                //data: handsontable.getDataAtRow(change[0][0])

                complete: function(data) {
                    $console.text('Autosaved (' + change.length + ' ' +
                            'cell' + (change.length > 1 ? 's' : '') + ')');
                    autosaveNotification = setTimeout(function() {
                        $console.text('Changes will be autosaved');
                    }, 1000);
                }
            });
        }

    }
//    ,
//    beforeRemoveRow: function(change) {
//        console.log(handsontable);
//
//    }
});
var handsontable = $container.data('handsontable');


$parent.find('a[name=roster]').click(function() {
//    $('button[name=save]').show();
    if (dynamicColumns.length != 5) {
        dynamicColumns = dynamicColumns.slice(0, 5);

    }
    $container.handsontable("updateSettings", {columns: dynamicColumns, readOnly: false});
    handsontable.render();

    $.ajax({
        url: "mongo",
        dataType: 'json',
        type: 'GET',
        data: {table: $(this).text()},
        success: function(res) {
            $console.text('Data loaded');
            handsontable.loadData(res.data);
//            console.log(res);
        }
    });
});


$parent.find('a[name=loadhwscore]').click(function() {
    dynamicColumns = dynamicColumns.slice(0, 5);
//    $('button[name=save]').hide();
    if (dynamicColumns.length == 5) {
        for (var i = 1; i <= 5; i++) {
            col = new Object();
            col.data = "total_hw_" + i.toString();
            col.title = "HW" + i.toString();
            col.type = "numeric";

            dynamicColumns.push(col);
        }
    }
    $container.handsontable("updateSettings", {columns: dynamicColumns, readOnly: true});
    handsontable.render();

    $.ajax({
        url: "mongomerge",
        dataType: 'json',
        type: 'GET',
        success: function(res) {
            $console.text('Data loaded');
            handsontable.loadData(res.data);
        }
    });
});

$parent.find('a[name=loaddiscscore]').click(function() {
    dynamicColumns = dynamicColumns.slice(0, 5);
    if (dynamicColumns.length == 5) {
        for (var i = 1; i <= 5; i++) {
            col = new Object();
            col.data = "total_quiz_" + i.toString();
            col.title = "Quiz " + i.toString();
            col.type = "numeric";

            dynamicColumns.push(col);
        }
    }
    $container.handsontable("updateSettings", {columns: dynamicColumns, readOnly: false});
    handsontable.render();

    $.ajax({
        url: "mongo",
        dataType: 'json',
        type: 'GET',
        data: {table: $(this).text()},
        success: function(res) {
            $console.text('Data loaded');
            handsontable.loadData(res.data);
        }
    });
});

$parent.find('a[name=loadlabscore]').click(function() {
    dynamicColumns = dynamicColumns.slice(0, 5);
    if (dynamicColumns.length == 5) {
        for (var i = 1; i <= 9; i++) {
            col = new Object();
            col.data = "total_lab_" + i.toString();
            col.title = "Lab " + i.toString();
            col.type = "numeric";

            dynamicColumns.push(col);
        }
    }
    $container.handsontable("updateSettings", {columns: dynamicColumns, readOnly: false});
    handsontable.render();

    $.ajax({
        url: "mongo",
        dataType: 'json',
        type: 'GET',
        data: {table: $(this).text()},
        success: function(res) {
            $console.text('Data loaded');
            handsontable.loadData(res.data);
        }
    });
});


$parent.find('button[name=save]').click(function() {
    var table = $(".nav-tabs li.active a").text();
    $.ajax({
        url: "mongosave",
        data: JSON.stringify({
            saveall: true,
            table: table,
            val: handsontable.getData()

        }),
        // data: {"data": handsontable.getData()}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function(res) {

            if (res.result === 'ok') {
                $console.text('Data saved');
            }
            else {
                $console.text('Save error');
            }
        },
        error: function() {
            $console.text('Save error!!!');
        }
    });
});

$parent.find('input[name=autosave]').click(function() {
    if ($(this).is(':checked')) {
        $console.text('Changes will be autosaved');
    }
    else {
        $console.text('Changes will not be autosaved');
    }
});

