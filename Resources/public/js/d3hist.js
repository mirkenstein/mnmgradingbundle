function plotHist(searchVal, values, title) {
    var searchVal = searchVal;
    var xMin = d3.min(values, function (d) {
        return Math.floor(d) - 10;
    });
    var xMax = d3.max(values, function (d) {
        return Math.floor(d) - 10;
    });

    // A formatter for counts.
    var formatCount = d3.format(",.0f");

    var margin = {top: 10, right: 30, bottom: 30, left: 30};

//        ,width = 960 - margin.left - margin.right,
//                height = 500 - margin.top - margin.bottom;
    var width = parseInt(d3.select(".histograms").style("width")) - margin.left - margin.right,
            height = parseInt(d3.select(".histograms").style("height")) - -margin.top - margin.bottom;

    var x = d3.scale.linear()
            .domain([xMin, xMax + 20])
            .range([0, width]);

    // Generate a histogram using twenty uniformly-spaced bins.
    var data = d3.layout.histogram()
            .bins(x.ticks(20))
            (values);

    var y = d3.scale.linear()
            .domain([0, d3.max(data, function (d) {
                    return d.y;
                })])
            .range([height, 0]);

    var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");

    var svg = d3.select(".histograms").append("div").attr("class", "hist").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var bar = svg.selectAll(".bar")
            .data(data)
            .enter().append("g")
            .attr("class", "bar")
            .attr("transform", function (d) {
                return "translate(" + x(d.x) + "," + y(d.y) + ")";
            });

    bar.append("rect").attr("fill", function (d) {

        return (d.filter(elementExists).length > 0 ? "orange" : "blue")
//            return (d.indexOf(88)>0 ? "orange" : "blue")
    })
            .attr("x", 1)
            .attr("width",
                    x(xMin + data[0].dx) - 1)
            .attr("height", function (d) {
                return height - y(d.y);
            });

    bar.append("text")
            .attr("dy", ".75em")
            .attr("y", 6)
            .attr("x", x(xMin + data[0].dx) / 2)
            .attr("text-anchor", "middle")
            .text(function (d) {
                return formatCount(d.y);
            });

    svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);


    svg.append("text")
            .attr("class", "chartlabel")
            .attr("x", (margin.left) * 4)
            .attr("y", (margin.top) * 2)
            .text(title + (searchVal > 0 ? searchVal : 0));


    function elementExists(element) {
        if (element == searchVal)
            return true;
        else
            return false;
    }


}

if (typeof(scores[0]) !== "undefined") {
    var score1 = scores[0].Exams.scores.exam_1,
         score3 = scores[0].Exams.scores.exam_3,
                     score2 = scores[0].Exams.scores.exam_2;
} else {
    var score1 = 0, score2 = 0;
}
plotHist(score1, histVals[0], "Your Exam 1 score ");
plotHist(score2, histVals[1], "Your Exam 2 score ");
plotHist(score3, histVals[2], "Your Exam 3 score ");

