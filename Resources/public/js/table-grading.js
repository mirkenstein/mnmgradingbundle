var $container = $("#example1");
var $console = $("#example1console");
var $parent = $container.parent();
var autosaveNotification;
var cellProperties = {};
var dynamicColumns = [];
if (typeof (roster[0]) === "undefined") {
    $console.text('You do not have assigned grading.');
} else {
    var rosterType = Object.keys(roster[0])[0];
}

var defaultColumns = [{
        data: "user.last_name",
        title: "Last Name "

    }, {
        data: "user.first_name",
        title: "First Name"

    }, {
        data: "user.username",
        title: "netid"
    }, {
        data: "user.UIN",
        type: 'numeric',
        title: "UIN"
    }, {
        data: rosterType+".CRN",
        type: 'numeric',
        title: "CRN"

    }];
dynamicColumns = defaultColumns;


function negativeValueRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    if (parseInt(value, 10) <= 0) { //if row contains negative number
        td.className = 'negative'; //add class "negative"
        td.style.color = 'red';
        td.style.background = '#EEE';
    }

    if (!value || value === '') {
        td.style.background = '#EEE';
    }

    if (value === 'Weight' || value === 'Total') {
        td.style.fontStyle = 'italic';
        td.style.background = 'cyan';
    }

}
Handsontable.renderers.registerRenderer('negativeValueRenderer', negativeValueRenderer); //maps function to lookup



if (rosterType == "Disc") {
    for (var i = 1; i <= 6; i++) {
        col = new Object();
        col.data = "Disc.scores.quiz_" + i.toString();
        col.title = "Quiz " + i.toString();
        col.type = "numeric";
        dynamicColumns.push(col);
    }
} else if (rosterType == "Labs") {

    for (var i = 1; i <= 9; i++) {
        col = new Object();
        col.data = "Labs.scores.lab_" + i.toString();
        col.title = "Lab " + i.toString();
        col.type = "numeric";
        dynamicColumns.push(col);
    }
}
$container.handsontable({
    startRows: 8,
    startCols: 6,
    rowHeaders: true,
    data: roster,
    minSpareRows: 1,
    extraCols: 1,
    contextMenu: false,
     columnSorting: true,       
    //columnSorting: {
     // column: 5,
    //  sortOrder: true
    //},
    cells: function (row, col, prop) {
        var cellProperties = {};
        cellProperties.renderer = "negativeValueRenderer"; //uses lookup map
        if (col < 5) {
            cellProperties.readOnly = true;
        }
        else
        {
            cellProperties.readOnly = false;
        }
        return cellProperties;
    },
    columns: dynamicColumns,
    afterChange: function (change, source) {
        if (source === 'loadData') {
            return; //don't save this change
        }
        var uniqueValues = [];
        $.each(change, function (i, v) {
            if ($.inArray(change[i][0], uniqueValues) === -1)
                uniqueValues.push(change[i][0]);
        });

        $.each(uniqueValues, function (i, v) {
            // var index=(handsontable.sortingEnabled ? handsontable.sortIndex[change[0][0]][0] : 0);
            // var index = handsontable.sortIndex[v][0];
            clearTimeout(autosaveNotification);
            $.ajax({
                url: "mongosave",
                dataType: "json",
                type: "POST",
                data: JSON.stringify({
                    saveall: false,
                    table: "Labs", //rosterType,
                    val: handsontable.getData()
//                    val: handsontable.getDataAtRow(index)
                }),
                contentType: "application/json",
                complete: function (data) {
                    $console.text('Autosaved (' + change.length + ' ' +
                            'cell' + (change.length > 1 ? 's' : '') + ')');
                    autosaveNotification = setTimeout(function () {
                        $console.text('Changes will be autosaved');
                    }, 1000);
                }
            });//end AJAX


        });


    }

});
var handsontable = $container.data('handsontable');
